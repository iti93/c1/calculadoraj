package com.example.calculadorajtrejo;


import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.calculadorajtrejo.R;

public class MainActivity extends AppCompatActivity {
    //inicializar variables
    private Button btnIngresar, btnRegresar;
    private EditText txtUsuario, txtContraseña;

    //componentes
    private void iniciarComponentes(){
        btnRegresar = findViewById(R.id.btnRegresar);
        btnIngresar = findViewById(R.id.btnIngresar);
        txtContraseña = findViewById(R.id.txtContraseña);
        txtUsuario = findViewById(R.id.txtUsuario);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        //Codificar el evento click
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });

    }
    private void ingresar(){
        String strUsuario  = "Trejo";
        String strContra = "mag0chis";

        if (txtUsuario.getText().toString().equals(strUsuario) && txtContraseña.getText().toString().equals(strContra)) {
            Bundle bundle = new Bundle();
            bundle.putString("usuario", strUsuario);

            Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Usuario o contraseña invalidos", Toast.LENGTH_SHORT).show();

        }
    }

    private void regresar(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("¿Desea regresar?");
        confirmar.setPositiveButton("Confirmar",(dialogInterface, which) -> finish());
        confirmar.setNegativeButton("Cancelar",(dialogInterface, which) -> finish());
        confirmar.show();

    }
}
