package com.example.calculadorajtrejo;

public class Calculadora {
    //DECLARAR VARIABLES
    private double  num1, num2;

    //CONSTRUCTOR
    public Calculadora(double num1, double num2) {
        this.num1 = num1;
        this.num2 = num2;
    }
    //FUNCIONES
    public double suma() {
        return num1+num2;
    }
    public double resta() {
        return num1-num2;
    }
    public double multiplicacion() {
        return num1*num2;
    }
    public double dividir() {
        return num1/num2;
    }

    public void setNum1(double num1) {
        this.num1 = num1;
    }

    public void setNum2(double num2) {
        this.num2 = num2;
    }
}
